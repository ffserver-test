#!/bin/bash

set -e

FFSERVER="ffserver/ffserver"
FFMPEG=~/ffmpeg-git/ffmpeg/ffmpeg-ref
TMP="."

rm /tmp/feed1.ffm >& /dev/null || true

$FFSERVER -f examples/ffserver-example2.conf >& $TMP/server.log &
pid2=$!
sleep 2
valgrind $FFMPEG -flags +bitexact  -i ~/videos/mm-short.mpg -y  -t 2 -acodec libmp3lame -flags +bitexact http://127.0.0.1:8192/feed1.ffm >& $TMP/encoder.log || cat $TMP/encoder.log

valgrind $FFSERVER -f examples/ffserver-ticket1986.conf >& $TMP/server2.log &
pid=$!
sleep 20
$FFMPEG -i rtsp://localhost:8554/h264-cut.mkv -flags +bitexact -y -t 1  out1986.avi >& $TMP/log3
kill $pid  $pid2 >& /dev/null || true

echo ok