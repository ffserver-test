#!/bin/bash

FFMPEG_SRC_PATH=~/ffmpeg-git/ffmpeg
BDIR=~/ffmpeg-git/ffmpeg
CMDUTILS_FLAGS="-lm"

FFSERVER_FLAGS="-g3  -Wdeclaration-after-statement -Wall -Wdisabled-optimization -Wpointer-arith -Wredundant-decls -Wwrite-strings -Wtype-limits -Wundef -Wmissing-prototypes -Wno-pointer-to-int-cast -Wstrict-prototypes -Wempty-body -Wno-parentheses -Wno-switch -Wno-format-zero-length -Wno-pointer-sign \
                -Werror=format-security -Werror=implicit-function-declaration -Werror=missing-prototypes -Werror=return-type -Werror=vla -Wformat \
                $(PKG_CONFIG_PATH=$FFMPEG_SRC_PATH/doc/examples/pc-uninstalled pkg-config --libs --cflags libavfilter libavformat libavdevice libavcodec libswscale libswresample libavutil)"

FFSERVER_SOURCES="$FFMPEG_SRC_PATH/cmdutils.o ffserver/ffserver.c ffserver/ffserver_config.c"

gcc -o ffserver/ffserver $FFSERVER_SOURCES $FFSERVER_FLAGS $CMDUTILS_FLAGS

#gcc $IDIR/ffplay.c -o ffserver/ffplay -I/usr/local/include/SDL2 $IDIR/cmdutils.o  -I$IDIR -I$BDIR -L$BDIR -lavfilter/libavfilter -lavutil/libavutil -lavformat/libavformat -lavdevice/libavdevice -lavcodec/libavcodec -lswscale/libswscale -lswresample/libswresample -lpostproc/libpostproc /usr/local/lib/libSDL2-2.0.so.0


#needed for exec:
#LD_LIBRARY_PATH=$BDIR/libavutil:$BDIR/libavcodec:$BDIR/libavdevice:$BDIR/libavformat:$BDIR/libavfilter:$BDIR/libswscale:$BDIR/libswresample:$BDIR/libpostproc$LD_LIBRARY_PATH ffserver/ffserver
