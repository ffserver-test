# Port on which the server is listening. You must select a different
# port from your standard HTTP web server if it is running on the same
# computer.
Port 8192

# Address on which the server is bound. Only useful if you have
# several network interfaces.
BindAddress 127.0.0.1

# Number of simultaneous HTTP connections that can be handled. It has
# to be defined *before* the MaxClients parameter, since it defines the
# MaxClients maximum limit.
MaxHTTPConnections 6

# Number of simultaneous requests that can be handled. Since FFServer
# is very fast, it is more likely that you will want to leave this high
# and use MaxBandwidth, below.
MaxClients 3

# This is the maximum amount of kbit/sec that you are prepared to
# consume when streaming to clients.
MaxBandwidth 10000

# Access log file (uses standard Apache log file format)
# '-' is the standard output.
CustomLog /etc/ffserver.log

# Suppress that if you want to launch ffserver as a daemon.
NoDaemon


##################################################################
# Definition of the live feeds. Each live feed contains one video
# and/or audio sequence coming from an ffmpeg encoder or another
# ffserver. This sequence may be encoded simultaneously with several
# codecs at several resolutions.

<Feed feed1.ffm>
	File /tmp/feed1.ffm
	FileMaxSize 10M
	ACL allow 127.0.0.1
</Feed>

<Feed feed2.ffm>
	File /tmp/feed2.ffm
	FileMaxSize 10M
	ACL allow 127.0.0.1
</Feed>

<Feed feed3.ffm>
	File /tmp/feed3.ffm
	FileMaxSize 10M
	ACL allow 127.0.0.1
</Feed>

<Feed feed4.ffm>
	File /tmp/feed4.ffm
	FileMaxSize 10M
	ACL allow 127.0.0.1
</Feed>

##################################################################
# Now you can define each stream which will be generated from the
# original audio and video stream. Each format has a filename (here
# 'test1.mpg'). FFServer will send this stream when answering a
# request containing this filename.

<Stream 79-1.flv>
	Feed feed1.ffm

	Format flv
	AVOptionVideo flags +global_header
	AVOptionAudio flags +global_header

	AudioBitRate 128
	AudioChannels 1
	AudioSampleRate 44100

	VideoBitRate 320
	#VideoBufferSize 40
	VideoFrameRate 25
	VideoSize qvga
	#VideoIntraOnly
	#VideoGopSize 12

# Choose your codecs:
#AudioCodec mp2
#VideoCodec mpeg1video

# Suppress audio
#NoAudio

# Suppress video
#NoVideo

#VideoQMin 3
#VideoQMax 31

# Set this to the number of seconds backwards in time to start. Note that
# most players will buffer 5-10 seconds of video, and also you need to allow
# for a keyframe to appear in the data stream.
#Preroll 15

# ACL:

# You can allow ranges of addresses (or single addresses)
#ACL ALLOW <first address>

# You can deny ranges of addresses (or single addresses)
#ACL DENY <first address>

# You can repeat the ACL allow/deny as often as you like. It is on a per
# stream basis. The first match defines the action. If there are no matches,
# then the default is the inverse of the last ACL statement.
#
# Thus 'ACL allow localhost' only allows access from localhost.
# 'ACL deny 1.0.0.0 1.255.255.255' would deny the whole of network 1 and
# allow everybody else.

</Stream>

<Stream 79-2.flv>
	Feed feed2.ffm
	Format flv
	AudioBitRate 128
	AudioChannels 1
	AudioSampleRate 44100
	VideoFrameRate 25
	VideoSize qvga
</Stream>

<Stream 79-3.flv>
	Feed feed3.ffm
	Format flv
	AudioBitRate 128
	AudioChannels 1
	AudioSampleRate 44100
	VideoFrameRate 25
	VideoSize qvga
</Stream>

<Stream 79-4.flv>
	Feed feed4.ffm
	Format flv
	AudioBitRate 128
	AudioChannels 1
	AudioSampleRate 44100
	VideoFrameRate 25
	VideoSize qvga
</Stream>


##################################################################
# RTSP examples
#
# You can access this stream with the RTSP URL:
#   rtsp://localhost:5454/test1-rtsp.mpg
#
# A non-standard RTSP redirector is also created. Its URL is:
#   http://localhost:8090/test1-rtsp.rtsp

#<Stream test1-rtsp.mpg>
#Format rtp
#File "/usr/local/httpd/htdocs/test1.mpg"
#</Stream>


# Transcode an incoming live feed to another live feed,
# using libx264 and video presets

#<Stream live.h264>
#Format rtp
#Feed feed1.ffm
#VideoCodec libx264
#VideoFrameRate 24
#VideoBitRate 100
#VideoSize 480x272
#AVPresetVideo default
#AVPresetVideo baseline
#AVOptionVideo flags +global_header
#
#AudioCodec libfaac
#AudioBitRate 32
#AudioChannels 2
#AudioSampleRate 22050
#AVOptionAudio flags +global_header
#</Stream>

##################################################################
# Special streams

# Server status

<Stream stat.html>
Format status

# Only allow local people to get the status
ACL allow localhost
#ACL allow 192.168.0.0 192.168.255.255

#FaviconURL http://pond1.gladstonefamily.net:8080/favicon.ico
</Stream>


# Redirect index.html to the appropriate site

#<Redirect index.html>
#URL http://www.ffmpeg.org/
#</Redirect>
